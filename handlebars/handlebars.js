const express = require('express')
const app = express()
const handlebars = require('express-handlebars')

// Configura o express para usar o handlebars como template engine
app.engine('handlebars', handlebars({defaultLayout: 'main'})) // main = views/layouts/main.handlebars
app.set('view engine', 'handlebars')

// Criando rotas
app.get('/', (req, res) => {
    res.render('page'); // retorna o views/page.handlebars, que estende o views/layouts/main.handlebars
})

// Levantando o servidor
app.listen(8081, () => {
    console.log('Servidor rodando')
})