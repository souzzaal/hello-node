const express = require('express')
router = express.Router()

router.use((req, res, next) => {
    console.log('passando pelo middleware generico para todas as rotas do modulo')
    next()
})

router.get('/', (req, res)=>{
    res.send('rota HOME de usuarios')
})

router
    .use('/:id/cars', (req, res, next) => {
        console.log('passando pelo middleware específico para a rota')
        if (req.params.id > 0 && req.params.id < 5) {
            next()
        } else {
            res.send('Usuário não encontrado');
        }
    })
    .get('/:id/cars', (req, res) => {
        res.send('sub-rota que lista os carros do usuario ' + req.params.id)
    })

module.exports = router;