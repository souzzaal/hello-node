const express = require('express')
const app = express()
const userRouter = require('./user-router')

const PORT = 8080;

/* ROTAS SIMPLES */

// Criando rotas
app.get('/', (req, res) => {
    res.send('Hello World NodeJS');
})

app.get('/sobre', (req, res) => {
    res.send('Pagina sobre');
})

app.get('/blog', (req, res) => {
    res.send('Blog');
})

// Rota com parâmetros
app.get('/ola/:nome', (req, res) => {
    res.send(`Ola, ${req.params.nome}`);
})

// Retornando um arquivo HTML
app.get('/html', (req, res) => {
    res.sendFile(__dirname + '/html/index.html')
})


/* AGRUPANDO ROTAS DE UM RECURSO COM O METODO ROUTE */

app.route('/books')
    .all((req, res, next) => {
        // all() será chamado em resposta a qualquer método HTTP. usado para carregar funções de middleware em um caminho específico para todos os métodos de solicitação.
        console.log('Acessando a rota books via ' + req.method);
        next(); // passa o controle para o próximo manipulador
    })
    .get((req, res) => {
        res.send('GET Books at ' + new Date())
    }).post((req, res) => {
        res.send('POST Books')
    }).patch((req, res) => {
        res.send('PATCH Book')
    }).put((req, res) => {
        res.send('PUT Book')
    }).delete((req, res) => {
        res.send('DELETE Book')
    })

/* ROTAS COM O EXPRESS ROUTER */

// Use a classe express.Router para criar manipuladores de rota modulares e montáveis. Uma instância de Router é um middleware e sistema de roteamento completo; por essa razão, ela é frequentemente referida como um “mini-aplicativo”
// Para testar acessar /users e /users/cars
app.use('/users', userRouter)


// Levantando o servidor
app.listen(PORT, () => {
    console.log(`Servidor rodando na porta ${PORT}`)
})