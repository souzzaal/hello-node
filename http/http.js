const http = require('http')

const porta = 8080;

// criando server
http.createServer((req, res) => {
    if (req.url == '/sobre') {
        res.end('<h1> Sobre nós </h1>')
    } else {
        res.end('<h1> Home </h1>')
    }
}).listen(porta)

console.log(`Servidor ouvindo na porta ${porta}`);
