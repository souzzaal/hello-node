const Sequelize = require('sequelize')

// Criando a conexão com o banco
sequelize = new Sequelize('node', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
})

// Testando a autenticacao
sequelize.authenticate().then(()=>{
    console.log('Conectado com sucesso');
}).catch(erro => {
    console.log(`Erro` + erro);
})

// Criando Models
const Postagem = sequelize.define('postagens', {
    titulo: {
        type: Sequelize.STRING
    },
    conteudo: {
        type: Sequelize.TEXT
    }
})

const Usuario = sequelize.define('usuarios', {
    nome: {
        type: Sequelize.STRING
    },
    sobrenome: {
        type: Sequelize.STRING
    },
    idade: {
        type: Sequelize.SMALLINT
    },
    email: {
        type: Sequelize.STRING
    }
})


// Cria as tabelas no banco de dados de acordo com os modelos definidos
// Postagem.sync({force: true}); 
// Usuario.sync({force: true});


// Insere dados no banco
Postagem.create({
    titulo: 'Hello world, NodeJS',
    conteudo: 'Primeiro insert com Sequelize'
});

Usuario.create({
    nome: 'André Luiz',
    sobrenome: 'CS',
    idade: 32,
    email: 'andre@mail.com'
})

